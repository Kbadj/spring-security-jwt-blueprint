package dim.kompo.security.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.Claims;

public class JwtAuthenticationFilter extends OncePerRequestFilter {

	private final JwtTokenProvider tokenProvider;

	private final CustomUserDetailsService customUserDetailsService;

	public JwtAuthenticationFilter(final JwtTokenProvider tokenProvider,
			final CustomUserDetailsService customUserDetailsService) {
		this.tokenProvider = tokenProvider;
		this.customUserDetailsService = customUserDetailsService;
	}

	private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationFilter.class);

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		try {
			String jwt = getJwtFromRequest(request);

			if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {

				UserPrincipal userPrincipal = getUserPrincipalFromJwt(jwt);
				UsernamePasswordAuthenticationToken authentication =
						new UsernamePasswordAuthenticationToken(userPrincipal, null, userPrincipal.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
		} catch (Exception ex) {
			logger.error("Could not set user authentication in security context", ex);
		}

		filterChain.doFilter(request, response);
	}

	private String getJwtFromRequest(HttpServletRequest request) {
		String bearerToken = request.getHeader("Authorization");
		if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7, bearerToken.length());
		}
		return null;
	}

	private UserPrincipal getUserPrincipalFromJwt(final String jwt) {
		Claims jwtClaims = tokenProvider.getJwtClaims(jwt);
		Long userId = Long.parseLong(jwtClaims.getSubject());
		String username = jwtClaims.get("username").toString();
		String email = jwtClaims.get("email").toString();
		List<Map<String, String>> authorities = (ArrayList<Map<String, String>>) jwtClaims.get("authorities");
		String authority = authorities.get(0).get("authority");
                /*
                    Note that you could also encode the user's username and roles inside JWT claims
                    and create the UserDetails object by parsing those claims from the JWT.
                    That would avoid the following database hit. It's completely up to you.
                 */
//				UserDetails userDetails = customUserDetailsService.loadUserById(userId);
		return UserPrincipal.of(userId, username, email, authority);

	}
}